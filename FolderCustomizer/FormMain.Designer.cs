﻿namespace FolderCustomizer
{
    partial class FormMain
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Liberare le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.textBoxFolderPath = new System.Windows.Forms.TextBox();
            this.flowLayoutPanelIcons = new System.Windows.Forms.FlowLayoutPanel();
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.checkEditHidden = new DevExpress.XtraEditors.CheckEdit();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.barCheckItemWithExtension = new DevExpress.XtraBars.BarCheckItem();
            this.barCheckItemOnlyFilesName = new DevExpress.XtraBars.BarCheckItem();
            this.checkEditReadOnly = new DevExpress.XtraEditors.CheckEdit();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.dropDownButtonCopyList = new DevExpress.XtraEditors.DropDownButton();
            this.popupMenu1 = new DevExpress.XtraBars.PopupMenu(this.components);
            this.checkEditSubFolder = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditShowDesktopIni = new DevExpress.XtraEditors.CheckEdit();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.myFileBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colPath = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExtension = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFolder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHasException = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.progressBarControl1 = new DevExpress.XtraEditors.ProgressBarControl();
            this.simpleButtonDeleteEmptySubFolders = new DevExpress.XtraEditors.SimpleButton();
            this.ButtonFolderCreate = new DevExpress.XtraEditors.SimpleButton();
            this.ButtonResetFolder = new DevExpress.XtraEditors.SimpleButton();
            this.ButtonApply = new DevExpress.XtraEditors.SimpleButton();
            this.pictureEditFolderImage = new DevExpress.XtraEditors.PictureEdit();
            this.textBoxFolderComment = new System.Windows.Forms.TextBox();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.tabbedControlGroupIcons = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroupIcons = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupFiles = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.memoEditDesctopIni = new DevExpress.XtraEditors.MemoEdit();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditHidden.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditReadOnly.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditSubFolder.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditShowDesktopIni.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.myFileBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.progressBarControl1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEditFolderImage.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroupIcons)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupIcons)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupFiles)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditDesctopIni.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            this.SuspendLayout();
            // 
            // textBoxFolderPath
            // 
            this.textBoxFolderPath.Location = new System.Drawing.Point(82, 5);
            this.textBoxFolderPath.Name = "textBoxFolderPath";
            this.textBoxFolderPath.Size = new System.Drawing.Size(765, 20);
            this.textBoxFolderPath.TabIndex = 0;
            this.textBoxFolderPath.TextChanged += new System.EventHandler(this.textBoxFolderPath_TextChanged);
            // 
            // flowLayoutPanelIcons
            // 
            this.flowLayoutPanelIcons.AutoScroll = true;
            this.flowLayoutPanelIcons.BackColor = System.Drawing.Color.White;
            this.flowLayoutPanelIcons.Location = new System.Drawing.Point(277, 120);
            this.flowLayoutPanelIcons.Margin = new System.Windows.Forms.Padding(1);
            this.flowLayoutPanelIcons.Name = "flowLayoutPanelIcons";
            this.flowLayoutPanelIcons.Padding = new System.Windows.Forms.Padding(3);
            this.flowLayoutPanelIcons.Size = new System.Drawing.Size(612, 306);
            this.flowLayoutPanelIcons.TabIndex = 4;
            // 
            // treeView1
            // 
            this.treeView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.treeView1.Location = new System.Drawing.Point(11, 120);
            this.treeView1.Name = "treeView1";
            this.treeView1.Size = new System.Drawing.Size(262, 306);
            this.treeView1.TabIndex = 5;
            this.treeView1.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.treeView1_NodeMouseClick);
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.memoEditDesctopIni);
            this.layoutControl1.Controls.Add(this.checkEditHidden);
            this.layoutControl1.Controls.Add(this.checkEditReadOnly);
            this.layoutControl1.Controls.Add(this.panelControl2);
            this.layoutControl1.Controls.Add(this.checkEditSubFolder);
            this.layoutControl1.Controls.Add(this.checkEditShowDesktopIni);
            this.layoutControl1.Controls.Add(this.gridControl1);
            this.layoutControl1.Controls.Add(this.panelControl1);
            this.layoutControl1.Controls.Add(this.pictureEditFolderImage);
            this.layoutControl1.Controls.Add(this.treeView1);
            this.layoutControl1.Controls.Add(this.flowLayoutPanelIcons);
            this.layoutControl1.Controls.Add(this.textBoxFolderComment);
            this.layoutControl1.Controls.Add(this.textBoxFolderPath);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1020, 222, 632, 656);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(900, 467);
            this.layoutControl1.TabIndex = 7;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // checkEditHidden
            // 
            this.checkEditHidden.Location = new System.Drawing.Point(105, 53);
            this.checkEditHidden.MenuManager = this.barManager1;
            this.checkEditHidden.Name = "checkEditHidden";
            this.checkEditHidden.Properties.Caption = "Hidden";
            this.checkEditHidden.Size = new System.Drawing.Size(109, 19);
            this.checkEditHidden.StyleController = this.layoutControl1;
            this.checkEditHidden.TabIndex = 15;
            this.checkEditHidden.CheckedChanged += new System.EventHandler(this.checkEditHidden_CheckedChanged);
            // 
            // barManager1
            // 
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barCheckItemWithExtension,
            this.barCheckItemOnlyFilesName});
            this.barManager1.MaxItemId = 4;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(900, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 467);
            this.barDockControlBottom.Size = new System.Drawing.Size(900, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 467);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(900, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 467);
            // 
            // barCheckItemWithExtension
            // 
            this.barCheckItemWithExtension.Caption = "Con estensione";
            this.barCheckItemWithExtension.Checked = true;
            this.barCheckItemWithExtension.Id = 2;
            this.barCheckItemWithExtension.Name = "barCheckItemWithExtension";
            // 
            // barCheckItemOnlyFilesName
            // 
            this.barCheckItemOnlyFilesName.Caption = "Solo nomi file";
            this.barCheckItemOnlyFilesName.Id = 3;
            this.barCheckItemOnlyFilesName.Name = "barCheckItemOnlyFilesName";
            // 
            // checkEditReadOnly
            // 
            this.checkEditReadOnly.Location = new System.Drawing.Point(5, 53);
            this.checkEditReadOnly.MenuManager = this.barManager1;
            this.checkEditReadOnly.Name = "checkEditReadOnly";
            this.checkEditReadOnly.Properties.Caption = "ReadOnly ";
            this.checkEditReadOnly.Size = new System.Drawing.Size(96, 19);
            this.checkEditReadOnly.StyleController = this.layoutControl1;
            this.checkEditReadOnly.TabIndex = 14;
            this.checkEditReadOnly.CheckedChanged += new System.EventHandler(this.checkEditReadOnly_CheckedChanged);
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.dropDownButtonCopyList);
            this.panelControl2.Location = new System.Drawing.Point(11, 400);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(878, 26);
            this.panelControl2.TabIndex = 13;
            // 
            // dropDownButtonCopyList
            // 
            this.dropDownButtonCopyList.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.dropDownButtonCopyList.DropDownControl = this.popupMenu1;
            this.dropDownButtonCopyList.Location = new System.Drawing.Point(759, 3);
            this.dropDownButtonCopyList.MenuManager = this.barManager1;
            this.dropDownButtonCopyList.Name = "dropDownButtonCopyList";
            this.dropDownButtonCopyList.Size = new System.Drawing.Size(114, 20);
            this.dropDownButtonCopyList.TabIndex = 2;
            this.dropDownButtonCopyList.Text = "Copia lista file";
            this.dropDownButtonCopyList.Click += new System.EventHandler(this.dropDownButtonCopyList_Click);
            // 
            // popupMenu1
            // 
            this.popupMenu1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barCheckItemWithExtension, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barCheckItemOnlyFilesName)});
            this.popupMenu1.Manager = this.barManager1;
            this.popupMenu1.Name = "popupMenu1";
            // 
            // checkEditSubFolder
            // 
            this.checkEditSubFolder.EditValue = true;
            this.checkEditSubFolder.Location = new System.Drawing.Point(141, 104);
            this.checkEditSubFolder.Name = "checkEditSubFolder";
            this.checkEditSubFolder.Properties.Caption = "Sotto cartelle";
            this.checkEditSubFolder.Size = new System.Drawing.Size(748, 19);
            this.checkEditSubFolder.StyleController = this.layoutControl1;
            this.checkEditSubFolder.TabIndex = 12;
            this.checkEditSubFolder.CheckedChanged += new System.EventHandler(this.checkEditSubFolder_CheckedChanged);
            // 
            // checkEditShowDesktopIni
            // 
            this.checkEditShowDesktopIni.Location = new System.Drawing.Point(11, 104);
            this.checkEditShowDesktopIni.Name = "checkEditShowDesktopIni";
            this.checkEditShowDesktopIni.Properties.Caption = "Show Desktop.ini";
            this.checkEditShowDesktopIni.Size = new System.Drawing.Size(126, 19);
            this.checkEditShowDesktopIni.StyleController = this.layoutControl1;
            this.checkEditShowDesktopIni.TabIndex = 11;
            this.checkEditShowDesktopIni.CheckedChanged += new System.EventHandler(this.checkEditShowDesktopIni_CheckedChanged);
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.myFileBindingSource;
            this.gridControl1.Location = new System.Drawing.Point(11, 127);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1});
            this.gridControl1.Size = new System.Drawing.Size(878, 269);
            this.gridControl1.TabIndex = 9;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colPath,
            this.colName,
            this.colExtension,
            this.colFolder,
            this.colHasException});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.GroupCount = 1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsView.ShowAutoFilterRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colFolder, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colPath
            // 
            this.colPath.FieldName = "Path";
            this.colPath.Name = "colPath";
            this.colPath.Visible = true;
            this.colPath.VisibleIndex = 1;
            this.colPath.Width = 188;
            // 
            // colName
            // 
            this.colName.FieldName = "Name";
            this.colName.Name = "colName";
            this.colName.Visible = true;
            this.colName.VisibleIndex = 2;
            this.colName.Width = 188;
            // 
            // colExtension
            // 
            this.colExtension.FieldName = "Extension";
            this.colExtension.Name = "colExtension";
            this.colExtension.Visible = true;
            this.colExtension.VisibleIndex = 3;
            this.colExtension.Width = 315;
            // 
            // colFolder
            // 
            this.colFolder.FieldName = "Folder";
            this.colFolder.Name = "colFolder";
            this.colFolder.Visible = true;
            this.colFolder.VisibleIndex = 3;
            // 
            // colHasException
            // 
            this.colHasException.Caption = "Exception";
            this.colHasException.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colHasException.FieldName = "HasException";
            this.colHasException.Name = "colHasException";
            this.colHasException.Visible = true;
            this.colHasException.VisibleIndex = 0;
            this.colHasException.Width = 60;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Controls.Add(this.progressBarControl1);
            this.panelControl1.Controls.Add(this.simpleButtonDeleteEmptySubFolders);
            this.panelControl1.Controls.Add(this.ButtonFolderCreate);
            this.panelControl1.Controls.Add(this.ButtonResetFolder);
            this.panelControl1.Controls.Add(this.ButtonApply);
            this.panelControl1.Location = new System.Drawing.Point(5, 436);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(890, 26);
            this.panelControl1.TabIndex = 0;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.labelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl1.Location = new System.Drawing.Point(181, 3);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(128, 20);
            this.labelControl1.TabIndex = 4;
            this.labelControl1.Text = "Analisi delle cartelle";
            this.labelControl1.Visible = false;
            // 
            // progressBarControl1
            // 
            this.progressBarControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.progressBarControl1.EditValue = 50;
            this.progressBarControl1.Location = new System.Drawing.Point(315, 4);
            this.progressBarControl1.MenuManager = this.barManager1;
            this.progressBarControl1.Name = "progressBarControl1";
            this.progressBarControl1.Properties.ShowTitle = true;
            this.progressBarControl1.Properties.Tag = "";
            this.progressBarControl1.Size = new System.Drawing.Size(366, 18);
            this.progressBarControl1.TabIndex = 3;
            this.progressBarControl1.Visible = false;
            // 
            // simpleButtonDeleteEmptySubFolders
            // 
            this.simpleButtonDeleteEmptySubFolders.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.simpleButtonDeleteEmptySubFolders.Location = new System.Drawing.Point(5, 3);
            this.simpleButtonDeleteEmptySubFolders.Name = "simpleButtonDeleteEmptySubFolders";
            this.simpleButtonDeleteEmptySubFolders.Size = new System.Drawing.Size(169, 20);
            this.simpleButtonDeleteEmptySubFolders.TabIndex = 2;
            this.simpleButtonDeleteEmptySubFolders.Text = "Elimina le sotto cartelle vuote";
            this.simpleButtonDeleteEmptySubFolders.Click += new System.EventHandler(this.simpleButtonDeleteEmptySubFolders_Click);
            // 
            // ButtonFolderCreate
            // 
            this.ButtonFolderCreate.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.ButtonFolderCreate.Location = new System.Drawing.Point(687, 3);
            this.ButtonFolderCreate.Name = "ButtonFolderCreate";
            this.ButtonFolderCreate.Size = new System.Drawing.Size(62, 20);
            this.ButtonFolderCreate.TabIndex = 2;
            this.ButtonFolderCreate.Text = "Create";
            this.ButtonFolderCreate.Click += new System.EventHandler(this.buttonFolderCreate_Click);
            // 
            // ButtonResetFolder
            // 
            this.ButtonResetFolder.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.ButtonResetFolder.Location = new System.Drawing.Point(755, 3);
            this.ButtonResetFolder.Name = "ButtonResetFolder";
            this.ButtonResetFolder.Size = new System.Drawing.Size(62, 20);
            this.ButtonResetFolder.TabIndex = 1;
            this.ButtonResetFolder.Text = "Reset";
            this.ButtonResetFolder.Click += new System.EventHandler(this.buttonResetFolder_Click);
            // 
            // ButtonApply
            // 
            this.ButtonApply.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.ButtonApply.Location = new System.Drawing.Point(823, 3);
            this.ButtonApply.Name = "ButtonApply";
            this.ButtonApply.Size = new System.Drawing.Size(62, 20);
            this.ButtonApply.TabIndex = 0;
            this.ButtonApply.Text = "Applica";
            this.ButtonApply.Click += new System.EventHandler(this.buttonApply_Click);
            // 
            // pictureEditFolderImage
            // 
            this.pictureEditFolderImage.Location = new System.Drawing.Point(851, 5);
            this.pictureEditFolderImage.Name = "pictureEditFolderImage";
            this.pictureEditFolderImage.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEditFolderImage.Size = new System.Drawing.Size(44, 44);
            this.pictureEditFolderImage.StyleController = this.layoutControl1;
            this.pictureEditFolderImage.TabIndex = 8;
            this.pictureEditFolderImage.EditValueChanged += new System.EventHandler(this.pictureEdit1_EditValueChanged);
            // 
            // textBoxFolderComment
            // 
            this.textBoxFolderComment.Location = new System.Drawing.Point(82, 29);
            this.textBoxFolderComment.Name = "textBoxFolderComment";
            this.textBoxFolderComment.Size = new System.Drawing.Size(765, 20);
            this.textBoxFolderComment.TabIndex = 8;
            this.textBoxFolderComment.TextChanged += new System.EventHandler(this.textBoxFolderComment_TextChanged);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.tabbedControlGroupIcons,
            this.emptySpaceItem1,
            this.layoutControlItem7,
            this.layoutControlItem12});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlGroup1.Size = new System.Drawing.Size(900, 467);
            this.layoutControlGroup1.Text = "Root";
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.textBoxFolderPath;
            this.layoutControlItem1.CustomizationFormText = "Folder";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(846, 24);
            this.layoutControlItem1.Text = "Folder";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(74, 13);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.textBoxFolderComment;
            this.layoutControlItem2.CustomizationFormText = "Commento";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(846, 24);
            this.layoutControlItem2.Text = "Commento";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(74, 13);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.pictureEditFolderImage;
            this.layoutControlItem5.CustomizationFormText = "layoutControlItem5";
            this.layoutControlItem5.Location = new System.Drawing.Point(846, 0);
            this.layoutControlItem5.MaxSize = new System.Drawing.Size(48, 48);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(48, 48);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(48, 48);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.Text = "layoutControlItem5";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextToControlDistance = 0;
            this.layoutControlItem5.TextVisible = false;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.panelControl1;
            this.layoutControlItem6.CustomizationFormText = "layoutControlItem6";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 431);
            this.layoutControlItem6.MaxSize = new System.Drawing.Size(0, 30);
            this.layoutControlItem6.MinSize = new System.Drawing.Size(1, 30);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(894, 30);
            this.layoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem6.Text = "layoutControlItem6";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextToControlDistance = 0;
            this.layoutControlItem6.TextVisible = false;
            // 
            // tabbedControlGroupIcons
            // 
            this.tabbedControlGroupIcons.CustomizationFormText = "Icons";
            this.tabbedControlGroupIcons.Location = new System.Drawing.Point(0, 71);
            this.tabbedControlGroupIcons.Name = "tabbedControlGroupIcons";
            this.tabbedControlGroupIcons.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.tabbedControlGroupIcons.SelectedTabPage = this.layoutControlGroup2;
            this.tabbedControlGroupIcons.SelectedTabPageIndex = 2;
            this.tabbedControlGroupIcons.Size = new System.Drawing.Size(894, 360);
            this.tabbedControlGroupIcons.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroupIcons,
            this.layoutControlGroupFiles,
            this.layoutControlGroup2});
            this.tabbedControlGroupIcons.Text = "Icons";
            this.tabbedControlGroupIcons.SelectedPageChanged += new DevExpress.XtraLayout.LayoutTabPageChangedEventHandler(this.tabbedControlGroupIcons_SelectedPageChanged);
            // 
            // layoutControlGroupIcons
            // 
            this.layoutControlGroupIcons.CustomizationFormText = "Icons";
            this.layoutControlGroupIcons.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem4,
            this.layoutControlItem3});
            this.layoutControlGroupIcons.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupIcons.Name = "layoutControlGroupIcons";
            this.layoutControlGroupIcons.Padding = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutControlGroupIcons.Size = new System.Drawing.Size(882, 326);
            this.layoutControlGroupIcons.Text = "Icons";
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.treeView1;
            this.layoutControlItem4.CustomizationFormText = "Risorse";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(266, 326);
            this.layoutControlItem4.Text = "Risorse";
            this.layoutControlItem4.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(74, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.flowLayoutPanelIcons;
            this.layoutControlItem3.CustomizationFormText = "Icone";
            this.layoutControlItem3.Location = new System.Drawing.Point(266, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(616, 326);
            this.layoutControlItem3.Text = "Icone";
            this.layoutControlItem3.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(74, 13);
            // 
            // layoutControlGroupFiles
            // 
            this.layoutControlGroupFiles.CustomizationFormText = "Files";
            this.layoutControlGroupFiles.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem8,
            this.layoutControlItem10,
            this.layoutControlItem9,
            this.layoutControlItem11});
            this.layoutControlGroupFiles.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupFiles.Name = "layoutControlGroupFiles";
            this.layoutControlGroupFiles.Size = new System.Drawing.Size(882, 326);
            this.layoutControlGroupFiles.Text = "Files";
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.gridControl1;
            this.layoutControlItem8.CustomizationFormText = "layoutControlItem8";
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 23);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(882, 273);
            this.layoutControlItem8.Text = "layoutControlItem8";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem8.TextToControlDistance = 0;
            this.layoutControlItem8.TextVisible = false;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.checkEditShowDesktopIni;
            this.layoutControlItem10.CustomizationFormText = "layoutControlItem10";
            this.layoutControlItem10.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem10.MaxSize = new System.Drawing.Size(130, 23);
            this.layoutControlItem10.MinSize = new System.Drawing.Size(130, 23);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(130, 23);
            this.layoutControlItem10.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem10.Text = "layoutControlItem10";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem10.TextToControlDistance = 0;
            this.layoutControlItem10.TextVisible = false;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.checkEditSubFolder;
            this.layoutControlItem9.CustomizationFormText = "layoutControlItem9";
            this.layoutControlItem9.Location = new System.Drawing.Point(130, 0);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(752, 23);
            this.layoutControlItem9.Text = "layoutControlItem9";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem9.TextToControlDistance = 0;
            this.layoutControlItem9.TextVisible = false;
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.panelControl2;
            this.layoutControlItem11.CustomizationFormText = "layoutControlItem11";
            this.layoutControlItem11.Location = new System.Drawing.Point(0, 296);
            this.layoutControlItem11.MaxSize = new System.Drawing.Size(0, 30);
            this.layoutControlItem11.MinSize = new System.Drawing.Size(206, 30);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(882, 30);
            this.layoutControlItem11.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem11.Text = "layoutControlItem11";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem11.TextToControlDistance = 0;
            this.layoutControlItem11.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(213, 48);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(681, 23);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.checkEditReadOnly;
            this.layoutControlItem7.CustomizationFormText = "layoutControlItem7";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(100, 23);
            this.layoutControlItem7.Text = "layoutControlItem7";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextToControlDistance = 0;
            this.layoutControlItem7.TextVisible = false;
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.checkEditHidden;
            this.layoutControlItem12.CustomizationFormText = "layoutControlItem12";
            this.layoutControlItem12.Location = new System.Drawing.Point(100, 48);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(113, 23);
            this.layoutControlItem12.Text = "layoutControlItem12";
            this.layoutControlItem12.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem12.TextToControlDistance = 0;
            this.layoutControlItem12.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "layoutControlGroup2";
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem13});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Size = new System.Drawing.Size(882, 326);
            this.layoutControlGroup2.Tag = "";
            this.layoutControlGroup2.Text = "Desktop.ini";
            // 
            // memoEditDesctopIni
            // 
            this.memoEditDesctopIni.Location = new System.Drawing.Point(11, 120);
            this.memoEditDesctopIni.MenuManager = this.barManager1;
            this.memoEditDesctopIni.Name = "memoEditDesctopIni";
            this.memoEditDesctopIni.Size = new System.Drawing.Size(878, 306);
            this.memoEditDesctopIni.StyleController = this.layoutControl1;
            this.memoEditDesctopIni.TabIndex = 16;
            this.memoEditDesctopIni.UseOptimizedRendering = true;
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.Control = this.memoEditDesctopIni;
            this.layoutControlItem13.CustomizationFormText = "Edit Desktop.ini";
            this.layoutControlItem13.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(882, 326);
            this.layoutControlItem13.Text = "Edit Desktop.ini";
            this.layoutControlItem13.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem13.TextSize = new System.Drawing.Size(74, 13);
            // 
            // FormMain
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(900, 467);
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.HelpButton = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormMain";
            this.Text = "Folder Customizer";
            this.Load += new System.EventHandler(this.formMain_Load);
            this.DragDrop += new System.Windows.Forms.DragEventHandler(this.formMain_DragDrop);
            this.DragEnter += new System.Windows.Forms.DragEventHandler(this.formMain_DragEnter);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEditHidden.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditReadOnly.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditSubFolder.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditShowDesktopIni.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.myFileBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.progressBarControl1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEditFolderImage.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroupIcons)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupIcons)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupFiles)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditDesctopIni.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxFolderPath;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanelIcons;
        private System.Windows.Forms.TreeView treeView1;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private System.Windows.Forms.TextBox textBoxFolderComment;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.SimpleButton ButtonApply;
        private DevExpress.XtraEditors.PictureEdit pictureEditFolderImage;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraEditors.SimpleButton ButtonResetFolder;
        private DevExpress.XtraEditors.SimpleButton ButtonFolderCreate;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroupIcons;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupIcons;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupFiles;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraGrid.Columns.GridColumn colPath;
        private DevExpress.XtraGrid.Columns.GridColumn colName;
        private DevExpress.XtraGrid.Columns.GridColumn colExtension;
        private DevExpress.XtraGrid.Columns.GridColumn colFolder;
        private DevExpress.XtraEditors.CheckEdit checkEditShowDesktopIni;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraEditors.CheckEdit checkEditSubFolder;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraEditors.DropDownButton dropDownButtonCopyList;
        private DevExpress.XtraBars.PopupMenu popupMenu1;
        private DevExpress.XtraBars.BarCheckItem barCheckItemWithExtension;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarCheckItem barCheckItemOnlyFilesName;
        private DevExpress.XtraEditors.SimpleButton simpleButtonDeleteEmptySubFolders;
        private System.Windows.Forms.BindingSource myFileBindingSource;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colHasException;
        private DevExpress.XtraEditors.ProgressBarControl progressBarControl1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.CheckEdit checkEditHidden;
        private DevExpress.XtraEditors.CheckEdit checkEditReadOnly;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraEditors.MemoEdit memoEditDesctopIni;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
    }
}

